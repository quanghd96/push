var express = require('express');
var app = express();
var path = require("path");
var mysql = require("mysql");
var http = require('http').Server(app);
var io = require('socket.io')(http);
var router = express.Router();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

var pool = mysql.createPool({
	connectionLimit: 100,
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'push',
	debug: false,
	multipleStatements: true
});


var { addNotification, seen, getListNotSeen, addUser, seenMulti, token, updateTokenUserSubcribe } = require("./db");
var routes = require("./router")(router, mysql, pool, io);

app.use('/', router);

http.listen(3000, function () {
	console.log("Listening on 3000");
});

io.on('connection', socket => {
	socket.on('getListNotSeen', data => {
		getListNotSeen(data, mysql, pool, (err, result) => {
			io.emit('notify not seen', { token: data.token, count: result.length })
		})
	})
	socket.on('new register', data => {
		let d = JSON.parse(data);
		addNotification(d, mysql, pool, (error, result) => {
			if (error) {
				console.log("error");
				io.emit('error');
			} else {
				console.log("success");
				d.idNotification = result;
				console.log(d.idNotification);
				io.emit("notify", d);
			}
		});
	});
	socket.on('admin login', data => {
		dataJson = JSON.parse(data);
		addUser(dataJson, mysql, pool, (error, result) => {
			if (error) {
				console.log("error");
				io.emit('error');
			} else {
				console.log("save user success");
				updateTokenUserSubcribe(result, dataJson.token, mysql, pool);
				io.emit('token', dataJson);
			}
		});
	});
	socket.on('seen', data => {
		seen(data, mysql, pool, (error, result) => {
			if (error) {
				io.emit('error');
			} else {
				console.log(data.token + " seen " + data.idNotification);
			}
		});
	});
	socket.on('seen multi', data => {
		seenMulti(data, mysql, pool, (error, result) => {
			if (error) {
				io.emit('error');
			} else {
				console.log(data.token + " seen all notification");
			}
		});
	});
	socket.on('request token', data => {
		token(data, mysql, pool, (error, result) => {
			if (error) {
				io.emit('error');
			} else {
				console.log(result);
				io.emit('token', result);
			}
		});
	});

});
