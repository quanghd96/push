function addNotification(data, mysql, pool, callback) {
	var self = this;
	pool.getConnection((err, connection) => {
		if (err) {
			connection.release();
			return callback(true, null);
		} else {
			//Luu thong bao
			var sqlQuery = "INSERT into ?? (??,??,??,??,??) VALUES (?,?,?,?,?); SELECT LAST_INSERT_ID() as id;";
			var inserts = ["notification", "title", "body", "link", "icon", "createdAt",
				data.title, data.body, data.link, data.icon, data.createdAt];
			sqlQuery = mysql.format(sqlQuery, inserts);
			connection.query(sqlQuery, (err, rows) => {
				if (err) {
					connection.release();
					return callback(true, null);
				} else {
					const idNotification = rows[1][0].id;
					//Luu vao bang usersubcribe
					var sqlQuery = "SELECT * FROM ?? WHERE idProject = ?";
					var inserts = ["user", data.idProject];
					sqlQuery = mysql.format(sqlQuery, inserts);
					connection.query(sqlQuery, (err, rows) => {
						if (err) {
							connection.release();
							return callback(true, null);
						} else {
							for (let i = 0; i < rows.length; i++) {
								const row = rows[i];
								var sqlQuery = "INSERT into ?? (??,??,??) VALUES (?,?,?)";
								var inserts = ["usersubcribe", "token", "idNotification", "seen", row.token, idNotification, 0];
								sqlQuery = mysql.format(sqlQuery, inserts);
								connection.query(sqlQuery, (err, rows) => {
									if (err) {
										connection.release();
										return callback(true, null);
									} else {
										connection.release();
										return callback(false, idNotification);
									}
								});
							}
						}
					});
				}
			});
		}
		connection.on('error', function (err) {
			return callback(true, null);
		});
	});
};

function addUser(data, mysql, pool, callback) {
	var self = this;
	pool.getConnection((err, connection) => {
		if (err) {
			connection.release();
			return callback(true, null);
		} else {
			var sqlQuery = "SELECT ?? FROM ?? WHERE ?? = ?; INSERT into ?? (??,??) VALUES (?,?) ON DUPLICATE KEY UPDATE ?? = ?";
			var inserts = ["token", "user", "id", data.id, "user", "id", "token", data.id, data.token, "token", data.token];
			sqlQuery = mysql.format(sqlQuery, inserts);
			console.log(sqlQuery);
			connection.query(sqlQuery, (err, rows) => {
				if (err) {
					connection.release();
					return callback(true, null);
				} else {
					connection.release();
					return callback(false, rows[0][0].token);
				}
			});
		}
		connection.on('error', function (err) {
			return callback(true, null);
		});
	});
};

function updateTokenUserSubcribe(oldToken, newToken, mysql, pool) {
	var self = this;
	pool.getConnection(function (err, connection) {
		if (err) {
			connection.release();
		} else {
			var sqlQuery = "UPDATE ?? SET ?? = ? WHERE ?? = ?";
			var inserts = ["usersubcribe", "token", newToken, "token", oldToken];
			sqlQuery = mysql.format(sqlQuery, inserts);
			console.log(sqlQuery);
			connection.query(sqlQuery, function (err, rows) {
				if (err) {
					connection.release();
				} else {
					connection.release();
				}
			});
		}
	});
};
function seen(data, mysql, pool, callback) {
	var self = this;
	pool.getConnection(function (err, connection) {
		if (err) {
			connection.release();
			return callback(true, null);
		} else {
			var sqlQuery = "UPDATE ?? SET ?? = ? WHERE ?? = ? AND ?? = ?";
			var inserts = ["usersubcribe", "seen", "1", "token", data.token, "idNotification", data.idNotification];
			sqlQuery = mysql.format(sqlQuery, inserts);
			connection.query(sqlQuery, function (err, rows) {
				if (err) {
					connection.release();
					return callback(true, null);
				} else {
					connection.release();
					return callback(false, null);
				}
			});
		}
		connection.on('error', function (err) {
			return callback(true, null);
		});
	});
};
function seenMulti(data, mysql, pool, callback) {
	var self = this;
	pool.getConnection(function (err, connection) {
		if (err) {
			connection.release();
			return callback(true, null);
		} else {
			var sqlQuery = "UPDATE ?? SET ?? = ? WHERE ?? = ?";
			var inserts = ["usersubcribe", "seen", "1", "token", data.token, "idNotification"];
			sqlQuery = mysql.format(sqlQuery, inserts);
			connection.query(sqlQuery, function (err, rows) {
				if (err) {
					connection.release();
					return callback(true, null);
				} else {
					connection.release();
					return callback(false, null);
				}
			});
		}
		connection.on('error', function (err) {
			return callback(true, null);
		});
	});
};
function getListNotSeen(data, mysql, pool, callback) {
	var self = this;
	pool.getConnection(function (err, connection) {
		if (err) {
			connection.release();
			return callback(true, null);
		} else {
			var sqlQuery = "SELECT * FROM ?? WHERE ?? = ? AND ?? = ?";
			var inserts = ["usersubcribe", "seen", 0, "token", data.token];
			sqlQuery = mysql.format(sqlQuery, inserts);
			console.log(sqlQuery);
			connection.query(sqlQuery, function (err, rows) {
				if (err) {
					connection.release();
					return callback(true, null);
				} else {
					connection.release();
					return callback(false, rows);
				}
			});
		}
		connection.on('error', function (err) {
			return callback(true, null);
		});
	});
};
function token(data, mysql, pool, callback) {
	var self = this;
	pool.getConnection(function (err, connection) {
		if (err) {
			connection.release();
			return callback(true, null);
		} else {
			var sqlQuery = "SELECT * FROM ?? WHERE ?? = ? AND ?? = ?";
			var inserts = ["user", "id", data.id, "idProject", data.idProject];
			sqlQuery = mysql.format(sqlQuery, inserts);
			console.log(sqlQuery);
			connection.query(sqlQuery, function (err, rows) {
				if (err) {
					connection.release();
					return callback(true, null);
				} else {
					connection.release();
					return callback(false, rows[0]);
				}
			});
		}
		connection.on('error', function (err) {
			return callback(true, null);
		});
	});
};

module.exports = {
	addNotification,
	seen,
	getListNotSeen,
	addUser,
	seenMulti,
	token,
	updateTokenUserSubcribe
}