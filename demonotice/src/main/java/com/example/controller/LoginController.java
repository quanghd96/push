package com.example.controller;

import com.example.model.Message;
import com.example.model.User;
import com.example.service.UserService;
import com.google.gson.Gson;
import io.socket.client.IO;
import io.socket.client.Socket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = {"/", "/login"}, method = RequestMethod.GET)
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public int authority(@Valid User user) {
        System.out.println(user);
        return 0;
    }


    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView registration() {
        ModelAndView modelAndView = new ModelAndView();
        User user = new User();
        modelAndView.addObject("user", user);
        modelAndView.setViewName("registration");
        return modelAndView;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public ModelAndView createNewUser(@Valid User user, BindingResult bindingResult) throws URISyntaxException {
        System.out.println(user);
        ModelAndView modelAndView = new ModelAndView();
        User userExists = userService.findUserByEmail(user.getEmail());
        if (userExists != null) {
            System.out.println("\n User Exist");
            bindingResult
                    .rejectValue("email", "error.user",
                            "There is already a user registered with the email provided");
        }
        if (bindingResult.hasErrors()) {
            modelAndView.setViewName("registration");
            System.out.println("\nRegister Fail");
        } else {
            userService.saveUser(user);
            modelAndView.addObject("successMessage", "User has been add successfully");
            modelAndView.addObject("user", new User());
            modelAndView.setViewName("registration");
            System.out.println("\n Success");
            //noti
            Socket socket = IO.socket("http://localhost:3000");
            socket
                    .on(Socket.EVENT_CONNECT, args -> {
                        System.out.println("socket connect");
                        String body = "Người dùng mới:\n" + user.getName() + " đăng ký với email:" + user.getEmail();
                        Message message = new Message();
                        message.setIdProject(0);
                        message.setBody(body);
                        message.setTitle("Người dùng đăng ký mới");
                        message.setIcon("https://image.flaticon.com/teams/slug/freepik.jpg");
                        message.setLink("http://localhost:8080");
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        message.setCreatedAt(simpleDateFormat.format(new Date()));
                        socket.emit("new register", new Gson().toJson(message));
                        System.out.println("socket disconnect");
                        socket.disconnect();
                    });
            socket.connect();
        }
        return modelAndView;
    }
}
