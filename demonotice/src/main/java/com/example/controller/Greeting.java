package com.example.controller;

import com.example.model.Role;
import com.example.model.User;
import com.example.model.UserNotification;
import com.example.service.UserService;
import com.google.gson.Gson;
import io.socket.client.IO;
import io.socket.client.Socket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.net.URISyntaxException;
import java.security.SecureRandom;
import java.util.Iterator;
import java.util.Set;

@Controller
public class Greeting {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/err")
    public ModelAndView errHandle() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("err");
        return modelAndView;
    }

    @RequestMapping(value = "/home")
    public ModelAndView landing() throws URISyntaxException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        System.out.println(user);
        Set<Role> roles = user.getRoles();
        Iterator<Role> iroles = roles.iterator();
        ModelAndView modelAndView = new ModelAndView();
        while (iroles.hasNext()) {
            Role temp = iroles.next();
            System.out.println(temp.getId() + temp.getRole());
            if (temp.getRole().equals("ADMIN")) {
                System.out.println("This is admin");
                modelAndView.addObject("user_id", user.getId());
                Socket socket = IO.socket("http://localhost:3000");
                socket
                        .on(Socket.EVENT_CONNECT, args -> {
                            System.out.println("socket connect");
                            UserNotification userNotification = new UserNotification();
                            userNotification.setId(user.getId());
                            userNotification.setToken(generateToken());
                            System.out.println(userNotification.getToken());
                            socket.emit("admin login", new Gson().toJson(userNotification));
                            System.out.println("socket disconnect");
                            socket.disconnect();
                        });
                socket.connect();
                modelAndView.addObject("userName", "Welcome " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");
                modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");
                modelAndView.setViewName("admin/home");
                return modelAndView;
            }
        }
        modelAndView.setViewName("home");
        return modelAndView;
    }

    private String generateToken() {
        SecureRandom random = new SecureRandom();
        long longToken = Math.abs(random.nextLong());
        return Long.toString(longToken, 16);
    }
}
