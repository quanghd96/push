package com.example.controller;

import com.example.model.User;
import com.example.repository.UserRepository;
import com.example.service.UserService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.List;

@Controller
public class AdminController {
    @Autowired
    private UserService userService;
    @Qualifier("userRepository")
    @Autowired
    private UserRepository userRepository;
    Gson gson = new Gson();

    @RequestMapping(value = "/admin/home", method = RequestMethod.GET)
    public ModelAndView home() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        List<User> users = userRepository.findAll();

        modelAndView.addObject("userName", "Welcome " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");
        modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");

        String json = gson.toJson(users);
        System.out.println(json);

        modelAndView.addObject("userList", json);
        modelAndView.addObject("userList", users);
//    modelAndView.a
        modelAndView.setViewName("admin/home");

        return modelAndView;
    }

    @RequestMapping(value = "/admin/", method = RequestMethod.GET)
    public ModelAndView admin() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        List<User> users = userRepository.findAll();

        modelAndView.addObject("userName", "Welcome " + user.getName() + " " + user.getLastName() + " (" + user.getEmail() + ")");
        modelAndView.addObject("adminMessage", "Content Available Only for Users with Admin Role");

        String json = gson.toJson(users);
//    System.out.println(json);

        modelAndView.addObject("userList", json);
        modelAndView.addObject("userListO", users);
//    modelAndView.a
        modelAndView.setViewName("admin/home");

        return modelAndView;
    }


    @RequestMapping(value = "/admin/add", method = RequestMethod.GET)
    public ModelAndView add() {
        User user = new User();
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        modelAndView.setViewName("admin/add");
        modelAndView.addObject(user);
        return modelAndView;
    }

    @RequestMapping(value = "/admin/modify/{id}", method = RequestMethod.GET)
    public ModelAndView modify(@PathVariable long id) {
        User user = userService.findUserById(id);
        ModelAndView modelAndView = new ModelAndView();
        if (user == null) {
            modelAndView.setViewName("admin/add");
            return modelAndView;
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        modelAndView.setViewName("admin/modify");
        modelAndView.addObject(user);
        return modelAndView;
    }

    @RequestMapping(value = "/admin/modify", method = RequestMethod.POST)
    public ModelAndView modify_(@Valid User user) {
        System.out.println("Modify " + user);
        ModelAndView modelAndView = new ModelAndView();
        User userExists = userService.findUserById(user.getId());
        if (userExists != null) {
            userService.saveUser(user);
            modelAndView.addObject("successMessage", "User has been modify successfully");
            modelAndView.addObject("user", new User());
            modelAndView.setViewName("admin/add");
        } else {

        }
        return modelAndView;

    }

    @RequestMapping(value = "/admin/remove/{id}", method = RequestMethod.GET)
    public ModelAndView remove(@PathVariable long id) {
        System.out.println(id);
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        userRepository.delete(id);
        modelAndView.setViewName("admin/remove");
        return modelAndView;

    }

    @RequestMapping(value = "/admin/search", method = RequestMethod.GET)
    public ModelAndView search() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        modelAndView.setViewName("admin/search");
        return modelAndView;
    }


    @RequestMapping(value = "/admin/add", method = RequestMethod.POST)
    public ModelAndView add_(@Valid User user) {
        System.out.println(user);
        ModelAndView modelAndView = new ModelAndView();
        User userExists = userService.findUserByEmail(user.getEmail());
        if (userExists != null) {
//			bindingResult
//					.rejectValue("email", "error.user",
//							"There is already a user registered with the email provided");
        }
//		if (bindingResult.hasErrors()) {
//			modelAndView.setViewName("admin/add");
//		} 
        else {
            userService.saveUser(user);
            modelAndView.addObject("successMessage", "User has been add successfully");
            modelAndView.addObject("user", new User());
            modelAndView.setViewName("admin/add");
        }
        return modelAndView;

    }

    @RequestMapping(value = "/admin/search", method = RequestMethod.POST)
    public ModelAndView search_() {
        ModelAndView modelAndView = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        modelAndView.setViewName("admin/search");
        return modelAndView;
    }


}
