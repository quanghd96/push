package com.example.model;

public class UserNotification {
    private long id;
    private String token;
    private int idProject;

    public UserNotification() {
    }

    public UserNotification(long id, String token, int idProject) {
        this.id = id;
        this.token = token;
        this.idProject = idProject;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getIdProject() {
        return idProject;
    }

    public void setIdProject(int idProject) {
        this.idProject = idProject;
    }
}
