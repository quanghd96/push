package com.example.service;

import com.example.model.User;

import java.util.List;

public interface UserService {
	public User findUserByEmail(String email);
	public User findUserById(long id);
	public void saveUser(User user);
	public List<User> findAll();
}
